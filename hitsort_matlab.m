%
% Initialize Dakel ZEDO/ZDaemon Hit Classifier engine for Matlab
%
% Copyright 2019 Dakel.cz, www.dakel.cz
% Copyright 2019 Michal Hanak, hanak@dakel.cz
%
% license: BSD-3-Clause
%


% last processed JSON-RPC call / debugging purposes
global g_hitsort_last_call;

% internal globals
global g_hitsort_globals;

if(~isfield(g_hitsort_globals, 'tcp'))

	[basepath,~,~] = fileparts(mfilename('fullpath'));

	% hitsort global functions
	addpath (strcat(basepath, '/hitsortm'));
	addpath (strcat(basepath, '/hitsortm/matlab'));

	% expecting jsonlab in the sibgling directory
	addpath (strcat(basepath, '/jsonlab'));

	g_hitsort_globals = struct( ...
		'tcp', tcpip('0.0.0.0', 40005, 'NetworkRole', 'server'), ...
		'recv_buff', '', ...
	    'basepath', basepath);

	clear basepath;
end

