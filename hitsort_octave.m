%
% Initialize Dakel ZEDO/ZDaemon Hit Classifier engine for Octave
%
% Copyright 2019 Dakel.cz, www.dakel.cz
% Copyright 2019 Michal Hanak, hanak@dakel.cz
%
% license: BSD-3-Clause
%

pkg load sockets;

[basepath,~,~] = fileparts(mfilename('fullpath'));

% hitsort global functions
addpath (strcat(basepath, '/hitsortm'));
addpath (strcat(basepath, '/hitsortm/octave'));

% expecting jsonlab in the sibgling directory
addpath (strcat(basepath, '/jsonlab'));

%
% globals
%

% last processed JSON-RPC call / debugging purposes
global g_hitsort_last_call;             

% internal globals
global g_hitsort_globals = struct(
    'basepath', basepath,
    'server_port', 40005, 
    'recv_buff', '', 
    'sock_listen', -1, 
    'sock_conn', -1);

clear basepath;
