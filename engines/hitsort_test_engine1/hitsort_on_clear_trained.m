function result = hitsort_on_clear_trained(params)
%
% The ZDaemon client calls the ClearTrained() method to reset all training data.
%
% Copyright 2019 Dakel.cz, www.dakel.cz
% Copyright 2019 Michal Hanak, hanak@dakel.cz
%
% license: BSD-3-Clause
%
% Input:
%     params.cookie - Custom string value stored in the project.
% 
% Output:
%     result.success - Boolean training operation result.
%     result.cookie - Optional value to store to project settings.
%

    result.success = true;
    fprintf('HitSorter now clears all training data\n');
end
