function result = hitsort_on_classify_hit(params)
%
% Example Hit clasification routine. Called by the framework automatically.
% The ZDaemon client calls the ClassifyHit() method to classify a single hit.
%
% Copyright 2019 Dakel.cz, www.dakel.cz
% Copyright 2019 Michal Hanak, hanak@dakel.cz
%
% license: BSD-3-Clause
%
% Input:
%     params.hit - Structure with hit parameters.
%     params.signal - Signal data in a raw format selected by the ZDaemon (if selected).
%     params.signal_real - Signal data re-calculated by hitsort_recv_call to standard Octave/Matlab vector in voltage units.
%     params.cookie - Custom string value stored in the project.
%
% Ouptut:
%     result.class - Resulting class number 1...N. Return -1 for error and 0 as pending if a hit class cannot 
%                    be determined at the moment but retrying the operation may be required.
%     result.cookie - Optional value to store to project settings.
% 

    global g_classes_count;

    result.class = mod(params.hit.id, g_classes_count) + 1; 
    result.cookie = str2double(params.cookie)+1;

    fprintf('Classifying hit id=%d %s <-- %d\n', params.hit.id, datestr(hit_time(params.hit.time)), result.class);
end
