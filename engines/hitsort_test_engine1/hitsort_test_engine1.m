%
% Main module representing the hit classifier engine. Loaded automatically by the
% framework as soon as a client connects and selects this engine. 
% Perform any one-time initialization actions here.
%
% Copyright 2019 Dakel.cz, www.dakel.cz
% Copyright 2019 Michal Hanak, hanak@dakel.cz
%
% license: BSD-3-Clause
%

fprintf('HitSort test engine 1 loaded!\n');

global g_classes_count;

g_classes_count = 0;
