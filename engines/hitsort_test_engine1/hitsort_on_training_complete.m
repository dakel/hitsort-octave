function result = hitsort_on_training_complete(params)
%
% The ZDaemon client calls the `TraininingComplete()` method when user is finished with providing
% training inputs. The subsequent `TrainClassifier()` calls still may occur to extend the 
% training set. If this happens, the new training set is also concluded by a `TraininingComplete()`
% call.
%
% Copyright 2019 Dakel.cz, www.dakel.cz
% Copyright 2019 Michal Hanak, hanak@dakel.cz
%
% license: BSD-3-Clause
% 
% Input:
%     params.cookie - Custom string value stored in the project.
% 
% Output:
%     result.success - Boolean training operation result.
%     result.cookie - Optional value to store to project settings.
%

    result.success = true; 
    fprintf('HitSort training is now complete\n');
end
