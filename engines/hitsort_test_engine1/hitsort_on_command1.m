function result = hitsort_on_command1(params)
%
% Example Hit custom user command. Called by the framework automatically.
%
% Copyright 2019 Dakel.cz, www.dakel.cz
% Copyright 2019 Michal Hanak, hanak@dakel.cz
%
% license: BSD-3-Clause
%
% input: 
%     params: structure with JSON-RPC parameters
%
% output:
%     result: structure with JSON-RPC result.
%
    result.success = true;
    fprintf('HitSorter user command1 received\n');
end
