function result = hitsort_on_initialize(params)
%
% The ZDaemon client calls the Initialize() method immediately after
% a connection is established. Also, the function is called when ZDaemon
% project is re-open or saved to different path.
%
% Copyright 2019 Dakel.cz, www.dakel.cz
% Copyright 2019 Michal Hanak, hanak@dakel.cz
%
% license: BSD-3-Clause
%
% Input:
%     params.project - Path to current ZDaemon project file.
%     params.name - Name of the Hit Sorter item.
%     params.engine - Name of the engine file to load.
%     params.cookie - Custom string value stored in the project.
%     params.classes - Array of class names (array length = number of classes).
%
% Output:
%     result.version - Hit Sorter API version identification. Must be set to 1.
%     result.success - Indicatin of successfull initialization.
%     result.message - Information/Error message to show to user.
%     result.cookie - Optional value to store to project settings.
%     result.custom.commands - array of commands (JSON-RPC method names) which ZDaemon will
%                     add to its UI as parameters (will appear as push-buttons in the UI).
%

    global g_classes_count;

    if(isfield(params, 'classes'))
        g_classes_count = length(params.classes);

        if(g_classes_count <= 0)
            g_classes_count = 1;
        end
    end

    fprintf('HitSort test engine initialized (using %d classes)!\n', g_classes_count);

    result.version = 1;
    result.success = true;
    result.cookie = 100;
    result.message = 'This is test engine only, not useful to do any real work';

    % see the hitsort_on_command1 and hitsort_on_command2 methods
    result.custom.commands = { 'command1', 'command2' };
end
