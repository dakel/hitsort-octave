# Matlab/Octave framework for creating Acoustic Emission hit classifiers compatible with Dakel ZEDO system

The Dakel ZEDO system and its ZDaemon control application may connect to hit classifier engine
over TCP and use JSON-RPC calls to get the engine to classify AE hits. The full JSON-API interface
is [described here](https://drive.google.com/open?id=1pIde_Qr7on7Xk_aGScpqabprVLs7gW7PZ1XhFUFx4rI).

This repository contains a JSON-RPC wrapper and a framework to create Hit Classifier engines 
in Matlab or Octave.

Contact info@dakel.cz for more details about advanced Acoustic Emission monitoring systems, AE sensors,
amplifiers and other equipment needed for a complete AE solutions.

## Installation

Use git to clone this repository, then initialize and update submodules. This work depends on 
the [JSONLab](https://github.com/fangq/jsonlab) - an open-source MATLAB/Octave JSON encoder 
and decoder.

```
git clone https://bitbucket.org/dakel/hitsort-octave.git

cd hitsort-octave
git submodule init
git submodule update
```

## Usage

### Configuring 

The repository contains two initialization scripts, one for Octave and the other for Matlab.
Edit the script to change default TCP server listening port which is `40005`. No other 
configuration should be needed.

### Initialization

Start the Octave or Matlab and `addpath` the repository path:

```
addpath [path-to-repository]
```

Run the initialization script

``` 
hitsort_octave
```

or

``` 
hitsort_matlab
```

The initialization scripts registers additional paths and enables the `hitsort` commands.
In most of the cases, it should be enough to start the `hitsort_run` or `hitsort_run_forever` 
script which starts the TCP server and keeps it running in an endless loop.

```
>> hitsort_run_forever
HitSort server listening on port 40005
```

When a client connects, JSON-RPC commands processing loop starts:

```
Receiving JSON-RPC call ...
```

The first call is typically the `Initialize()`. Causing the selected classifier engine to load its
main m-script file and call the `hitsort_on_initialize()` function.

```
Receiving JSON-RPC call done (221 bytes)
Hit Sorter "Sorter Client 1" requested by remote client
Adding path "c:\zedo\xapps\octave\hitsort/engines/hitsort_test_engine1"
Loading the engine file "hitsort_test_engine1.m"
HitSort test engine 1 loaded!
Initializing the engine
HitSort test engine initialized (using 8 classes)!
```

The seesion continues by `ClassifyHit` (-> `hitsort_on_classify_hit()`) or `TrainClassifier` (-> `hitsort_on_train_classifier()`) calls:

```
Receiving JSON-RPC call done (119184 bytes)
Classifying hit id=90 04-Apr-2018 11:01:09 <-- 3
```

or

```
Receiving JSON-RPC call done (128614 bytes)
Training classifier 1/1 hit id=94 04-Apr-2018 11:01:10 --> class 3
```

After each processing, the JSON-RPC loop returns to waiting for a new call:

```
Receiving JSON-RPC call ...
```

Disconnecting the client or calling the `Exit()` JSON-RPC method breaks the loop and terminates the session:

```
Receiving JSON-RPC call -connection lost
HitSort server stopped
Removing path "c:\zedo\xapps\octave\hitsort/engines/hitsort_test_engine1"
```

If `hitsort_run` command was used, the sessions ends and Octave/Matlab returns to the command prompt interface.
If `hitsort_run_forever` was used, the session loop re-starts automatically, and waits for another client to connect.

## Control Commands Reference

### **hitsort_start_server** - listen for client connections

Atomic function to create the listening socket and wait for the first incoming connection.

### **hitsort_stop_server** - close the listening socket

Atomic function to shutdown the listening socket.

### **hitsort_recv_call** - receive and process a single JSON-RPC call

Atomic function to wait for a JSON-RPC call and process it.

### **hitsort_run** - run the server session

A simple sequence of `hitsort_server_start`, `hitsort_recv_call` loop and `hitsort_server_stop` calls to process a single client session.

### **hitsort_run_forever** - run the multiple server sessions endlessly

A simple loop calling `hitsort_run` endlessly.

## Callback Commands Reference

### **hitsort_on_initialize** - Handle JSON-RPC Initialize() call

This function is called automatically when JSON-RPC `Initialize()` call is received. The 
function receives the full set of `params` as an input argument. The `result` structure
returned by this function is sent back to JSON-RPC client as a response. 

The ZDaemon client calls the `Initialize()` method immediately after a connection is established. 
Also, the function is called when ZDaemon project is re-open or saved to different path.

*Input:*

*  **params.project** - Path to current ZDaemon project file.
*  **params.name** - Name of the Hit Sorter item.
*  **params.engine** - Name of the engine file to load.
*  **params.cookie** - Custom string value stored in the project.
*  **params.classes** - Array of class names (array length = number of classes). 

*Output:*

*  **result.version** - Hit Sorter API version identification. Must be set to `1`.
*  **result.cookie** - Optional value to store to project settings.
*  **result.custom.commands** - array of commands (JSON-RPC method names) which ZDaemon 
                      will add to its UI as parameters (will appear as push-buttons in the UI).

### **hitsort_on_classify_hit** - Handle JSON-RPC ClassifyHit() call

This function is called automatically when JSON-RPC `ClassifyHit()` call is received. The 
function receives the full set of `params` as an input argument. The `result` structure
returned by this function is sent back to JSON-RPC client as a response. 

The ZDaemon client calls the `ClassifyHit()` method to classify a single hit.

*Input:*

*  **params.source** - Name of the hit provider unit
*  **params.hit** - Structure with hit parameters.
*  **params.signal** - Signal data in a raw format selected by the ZDaemon (if selected).
*  **params.signal_real** - Signal data re-calculated by `hitsort_recv_call` to standard Octave/Matlab vector in voltage units.
*  **params.cookie** - Custom string value stored in the project.

*Ouptut:*

*  **result.class** - Resulting class number 1..N. Return -1 for `error` and 0 as `pending` if a hit class cannot be 
                     determined at the moment but retrying the operation may be required.
*  **result.cookie** - Optional value to store to project settings.

### **hitsort_on_clear_trained** - Handle JSON-RPC ClearTrained() call

This function is called automatically when JSON-RPC `ClearTrained()` call is received. The 
function receives the full set of `params` as an input argument. The `result` structure
returned by this function is sent back to JSON-RPC client as a response. 

The ZDaemon client calls the `ClearTrained()` method to reset all training data.

*Input:*

*  **params.cookie** - Custom string value stored in the project.

*Output:*

*  **result.success** - Boolean training operation result.
*  **result.cookie** - Optional value to store to project settings.


### **hitsort_on_train_classifier** - Handle JSON-RPC TrainClassifier() call

This function is called automatically when JSON-RPC `ClassifyHit()` call is received. The 
function receives the full set of `params` as an input argument. The `result` structure
returned by this function is sent back to JSON-RPC client as a response. 

The ZDaemon client calls the `TrainClassifier()` method to assign the hit and given class number as a training value.

*Input:*

*  **params.source** - Name of the hit provider unit
*  **params.hit** - Structure with hit parameters
*  **params.signal** - Signal data in a raw format selected by the ZDaemon (if selected).
*  **params.signal_real** - Signal data re-calculated by `hitsort_recv_call` to standard Octave/Matlab vector in voltage units.
*  **params.train_class** - Value 1..N used as a training value
*  **params.cookie** - Custom string value stored in the project

*Output:*

*  **result.success** - Boolean training operation result.
*  **result.cookie** - Optional value to store to project settings.

### **hitsort_on_training_complete** - Handle JSON-RPC `TraininingComplete()` call

This function is called automatically when JSON-RPC `TraininingComplete()` call is received. The 
function receives the full set of `params` as an input argument. The `result` structure
returned by this function is sent back to JSON-RPC client as a response. 

The ZDaemon client calls the `TraininingComplete()` method when user is finished with providing
training inputs. The subsequent `TrainClassifier()` calls still may occur to extend the 
training set. If this happens, the new training set is also concluded by a `TraininingComplete()`
call.

*Input:*

*  **params.cookie** - Custom string value stored in the project.

*Output:*

*  **result.success** - Boolean training operation result.
*  **result.cookie** - Optional value to store to project settings.


## Utility Functions Reference

### **hit_time** - Convert JSON hit time value to Octave/Matlab native unit

Use to convert the `params.hit.time` value to valid time value.

## Remarks

### Example of **params.hit** JSON object

```
params.hit = {
    "id":  67,                         // hit identifier
    "time": 1.5228e+12,                // JavaScript time of hit start
    "gt_start": 576158462832154388,    // Nanosecond global time of hit start
    "gt_end": 576158462836022628,      // Nanosecond global time of hit end
    "ampv":  0.025929,                 // Amplitude in Voltage units
    "ampdb":  88.276,                  // Amplitude in dB_AE units
    "hthr":  0.0027689,                // Hit-detection threshold in Voltage units
    "saturated": 0,                    // Indicates if signal sample is valid (no saturation ocurred)
    "len":  3868000,                   // Hit length in nanoseconds
    "rt":  64200,                      // Hit risetime (time to maximum aplitude) in nanoseconds
    "energy":  0.000000094383,         // Total Hit energy in V^2*s units
    "rt_energy":  0.0000000044361,     // Energy of hit risetimg in V^2*s units
    "rms":  0.0049397,                 // RMS hit value in Voltage units
    "rt_rms":  0.0083125,              // RMS of hit risetime in Voltage units
    "asl":  0.0034178,                 // ASL hit value in Voltage units
    "rt_asl":  0.0067577,              // ASL of hit risetime in Voltage units
    "gain":  44,                       // Total amplifier gain in dB
    "smp_ns":  200,                    // Sampling step in nanoseconds
    "hcnt":  104,                      // Number of crossings over hit-detection threshold
    "rt_hcnt":  4,                     // Number of crossings over hit-detection threshold during risetime
    "ucnt": [29, 4],                   // Number of crossings over user-defined thresholds
    "fcnt": [143,111,77,52,29,22,16, 8, 4, 2, 1, 1, 1, 1, 0, 0] // Number of crossings over 16 fixed thresholds
}
```

### The **params.signal** JSON object

```
params.signal = {
    "rawadc":  true,      // true=> 2-byte signed integers, false=> double voltage values
    "format":  “hex”,     // one of: “hex”, “base64”, “arr”
    “step_ns”: 20,        // nanosecond step between numbers (sampling speed)
    “n_samples”: 200000,  // number of samples
    “n_first”: 1000,      // index first hit sample (first/last depend on ZDaemon settings)
    “n_last”: 198000,     // index last hit (may be > n_samples if signal is incomplete)
    “data”: “00001111222233334444....”
}
```
