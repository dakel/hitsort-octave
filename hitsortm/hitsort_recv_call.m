function got_call = hitsort_recv_call()
%
% receive one JSON-RPC call and process it
% returns true if known function has been processed, 
% returns false if Exit() or unknown function has been processed
%
% Copyright 2019 Dakel.cz, www.dakel.cz
% Copyright 2019 Michal Hanak, hanak@dakel.cz
%
% license: BSD-3-Clause
%
    global g_hitsort_last_call;
    global g_hitsort_globals;

    got_call = false;
    iter_str = '-\|/';
    iter_ix = 1;

    if(~hitsort_is_server_running())
        ffprintf('cannot receive json-rpc call, not connected\n');
        return
    end

    fprintf('Receiving JSON-RPC call  ');
    while(~got_call)
        size = hitsort_get_recv_size();
        if(size > 0)
            one = hitsort_recv_data(size);

            g_hitsort_globals.recv_buff = [g_hitsort_globals.recv_buff, one];

            % get length of one valid JSON record, 0 when incomplete
            jslen = findjson(g_hitsort_globals.recv_buff);

            if(jslen > 0)
                jsdata = g_hitsort_globals.recv_buff(1:jslen);
                g_hitsort_globals.recv_buff(1:jslen) = [];
                got_call = true;
            else
                fprintf('\b%s', iter_str(iter_ix));
                iter_ix = mod(iter_ix, length(iter_str))+1;
            end
        else
            if(hitsort_is_server_running())
                % Matlab needs this to keep the figure, menu and UI alive
                pause(1e-3);
            else
                fprintf('connection lost\n');
                return 
            end
        end
    end

    fprintf('\bdone (%d bytes)\n', length(jsdata));
    %fprintf('JSON: "%s"\n', jsdata);
    request = loadjson(jsdata);

    if(~isfield(request, 'method') || ~isfield(request, 'jsonrpc') || ~isfield(request, 'id'))
        fprintf("bad json-rpc call, skipping...\n");
        return;
    end        

    % work with empty parameters?
    if(~isfield(request, 'params'))
        request.params.dummy = true;
    end

    % translate signal to read voltage, when present in the parameters (otherwise set [])
    request.params.signal_real = hit_parse_signal(request.params);

    % debugging plot
    if(length(request.params.signal_real) > 0)
        plot(request.params.signal_real);
        refresh();
        drawnow();
    end

    % debugging purposes
    g_hitsort_last_call.request = request;

    % prepare response
    response.jsonrpc = request.jsonrpc;
    response.id = request.id;

    if(strcmp(request.method, 'Exit'))
        got_call = false;
        response.result.success = true;
        fprintf("Exit received\n");
    elseif(strcmp(request.method, 'Initialize'))
        response.result = hitsort_load_engine(request.params);
        % TODO: remember custom commands to handle them later
    elseif(strcmp(request.method, 'ClassifyHit'))
        response.result = hitsort_on_classify_hit(request.params);
    elseif(strcmp(request.method, 'TrainClassifier'))
        response.result = hitsort_on_train_classifier(request.params);
    elseif(strcmp(request.method, 'ClearTrained'))
        response.result = hitsort_on_clear_trained(request.params);
    elseif(strcmp(request.method, 'TrainingComplete'))
        response.result = hitsort_on_training_complete(request.params);
    elseif(hitsort_is_custom_command(request.method))
        response.result = feval(strcat('hitsort_on_', request.method), request.params);
    else
        fprintf('WARNING: Ignoring unknown method "%s"\n', request.method);
        response.error.code = -1;
        response.error.message = [ 'Unknown method ', request.method ];
    end

    % debugging purposes
    g_hitsort_last_call.response = response;

    % send response back to client
    output = savejson('', response, 'ForceRootName', 0, 'Compact', 1);
    hitsort_send_data(output);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% determine if given string is registerred as a user command

function yes = hitsort_is_custom_command(cmd)
    global g_hitsort_globals;
    yes = 0;

    if(isfield(g_hitsort_globals, 'custom_commands'))
        % note: find in cells is only possible in Matlab, not implemented in Octave
        % yes = find(g_hitsort_globals.custom_commands == cmd)
        for c=g_hitsort_globals.custom_commands
            if(strcmp(c{1}, cmd))
                yes = 1;
            end
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% engine load and initialization

function result = hitsort_load_engine(params)
    global g_hitsort_globals;

    % unregister all existing engine paths
    hitsort_rm_engine_paths();

    % initialize custom commands
    g_hitsort_globals.custom_commands = [];

    if(isfield(params, 'name'))
        fprintf('Hit Sorter "%s" requested by remote client\n', params.name);
    end

    % load engine file
    if(isfield(params, 'engine') && ~strcmp(params.engine, ''))
        engine = strrep(params.engine, ' ', '_');
        name = strcat('hitsort_', engine);

        % addpath to the engine folder
        path = strcat(g_hitsort_globals.basepath, '/engines/', name);
        fprintf('Adding path "%s"\n', path);
        addpath(path);

        % call the initialization function from the engine directory
        fprintf('Loading the engine file "%s.m"\n', name);
        eval(name);

        fprintf('Initializing the engine\n', name);
        result = hitsort_on_initialize(params);

        % remember custom commands returned by the initialization routine
        if(isfield(result, 'custom') && isfield(result.custom, 'commands'))
            g_hitsort_globals.custom_commands = result.custom.commands;
        end
    else
        fprintf('WARNING: the engine file not specified\n');
        result = [];
    end
end
