function hitsort_rm_engine_paths()
%
% Unregister all paths to engines
%
% Copyright 2019 Dakel.cz, www.dakel.cz
% Copyright 2019 Michal Hanak, hanak@dakel.cz
%
% license: BSD-3-Clause
%
    paths = strsplit(path, ';');

    for cell=paths
        p = cell{1};

        if(strfind (p, 'hitsort/engines') > 0)
            fprintf('Removing path "%s"\n', p);
            rmpath(p);
        end
    end
end
