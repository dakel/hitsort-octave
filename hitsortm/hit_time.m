function time = hit_time(jstime)
%
% convert JavaScript 'time' member to Octave time
% https://wiki.octave.org/Date/Time_functions
%
% Copyright 2019 Dakel.cz, www.dakel.cz
% Copyright 2019 Michal Hanak, hanak@dakel.cz
%
% license: BSD-3-Clause
%

    % jstime is time in milliseconds since Unix epoch 1.1.1970
    time = jstime / 1000 / 86400 + 719529;
end

