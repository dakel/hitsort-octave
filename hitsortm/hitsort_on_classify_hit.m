function result = hitsort_on_classify_hit(params)
%
% Default hit clasification routine (this call is typically overriden)
% The ZDaemon client calls the ClassifyHit() method to classify a single hit.
%
% Copyright 2019 Dakel.cz, www.dakel.cz
% Copyright 2019 Michal Hanak, hanak@dakel.cz
%
% license: BSD-3-Clause
%
% Input:
%     params.hit - Structure with hit parameters.
%     params.signal - Signal data in a raw format selected by the ZDaemon (if selected).
%     params.signal_real - Signal data re-calculated by hitsort_recv_call to standard Octave/Matlab vector in voltage units.
%     params.cookie - Custom string value stored in the project.
%
% Ouptut:
%     result.class - Resulting class number 1...N. Return -1 for error and 0 as pending if a hit class cannot 
%                    be determined at the moment but retrying the operation may be required.
%     result.cookie - Optional value to store to project settings.
% 

    result.class_id = 0; 
    fprintf('WARNING: you should override the "hitsort_on_classify_hit" function in your Hit Sorter engine\n');
end
