function ret = findjson(buff)
%
% Find length of single complete valid JSON record in given string buffer
% Return 0 when JSON record is incomplete.
% TODO: this is quite dumb, just looks for the last matching top-level parenthesis { ... } 
%
% Copyright 2019 Dakel.cz, www.dakel.cz
% Copyright 2019 Michal Hanak, hanak@dakel.cz
%
% license: BSD-3-Clause
%
    ret = 0;

    len = length(buff);
    if(len <= 0)
        return;
    end

    left = double('{');
    right = double('}');

    indicies = find(buff == left | buff == right);
    match = 0;

    for ret=indicies
        if(buff(ret) == left)
            match = match + 1;
        elseif(buff(ret) == right)
        	match = match - 1;
            if(match == 0)
                return
            end
        end
    end

    % incomplete JSON
    ret = 0;
end

