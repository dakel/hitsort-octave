function out = hit_parse_signal(params)
%
% Extract signal voltage data from JSON-RPC parameters 'hit' and 'signal'
%
% Copyright 2019 Dakel.cz, www.dakel.cz
% Copyright 2019 Michal Hanak, hanak@dakel.cz
%
% license: BSD-3-Clause
%

    out = [];

    if(~isfield(params, 'hit') || ~isfield(params, 'signal'))
        return
    end

    hit = params.hit;
    sig = params.signal;

    if(~isfield(sig, 'format') || ~isfield(sig, 'data') || ~isfield(sig, 'rawadc'))
        return 
    end

    if(strcmp(sig.format, 'hex'))
        if(sig.rawadc)
            out = hit_parse_signal_hexnum(sig.data, 4, 'int16');
        else
            out = hit_parse_signal_hexnum(sig.data, 16, 'double');
        end
    elseif(strcmp(sig.format, 'arr'))
        % no conversion needed, data already passed as array
        out = sig.data;
    else
        fprintf('ERROR: unknown signal format %s\n', sig.format);
        return 
    end

    % convert from ADC to voltage
    if(sig.rawadc)
        % 10.0 is maximum range, 16 is ADC resolution
        lsb = 10.0 * 10^(-1*params.hit.gain/20) / 2^16;
        out = out * lsb;
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Extract signal helper - string of HEX data, split to numbers per elem_size characters

function sig = hit_parse_signal_hexnum(string, elem_size, type)
    % we return the vector
    sig = [];

    % hex string length
    len = length(string);
    i = 1;
    while(i<len)
        v = swapbytes(hex2num(substr(string, i, elem_size), type));
        sig = [sig, v];
        i = i + elem_size;
    end
end

