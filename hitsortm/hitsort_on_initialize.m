function result = hitsort_on_initialize(params)
%
% Default engine initialization (this call is typically overriden).
% The ZDaemon client calls the Initialize() method immediately after 
% a connection is established. Also, the function is called when ZDaemon 
% project is re-open or saved to different path.
%
% Copyright 2019 Dakel.cz, www.dakel.cz
% Copyright 2019 Michal Hanak, hanak@dakel.cz
%
% license: BSD-3-Clause
% 
% Input:
%     params.project - Path to current ZDaemon project file.
%     params.name - Name of the Hit Sorter item.
%     params.engine - Name of the engine file to load.
%     params.cookie - Custom string value stored in the project.
%     params.classes - Array of class names (array length = number of classes).
%
% Output:
%     result.version - Hit Sorter API version identification. Must be set to 1.
%     result.cookie - Optional value to store to project settings.
%     result.custom.commands - array of commands (JSON-RPC method names) which ZDaemon will 
%                     add to its UI as parameters (will appear as push-buttons in the UI).
% 

    result.version = 1; 
    fprintf('WARNING: you should override the "hitsort_on_initialize" function in your Hit Sorter engine\n');
end
