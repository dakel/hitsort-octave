function hitsort_stop_server()
%
% Terminate server and close sockets.
%
% Copyright 2019 Dakel.cz, www.dakel.cz
% Copyright 2019 Michal Hanak, hanak@dakel.cz
%
% license: BSD-3-Clause
%
    global g_hitsort_globals;

    if(g_hitsort_globals.sock_conn > 0)
        disconnect(g_hitsort_globals.sock_conn);
        g_hitsort_globals.sock_conn = -1;
    end

    if(g_hitsort_globals.sock_listen > 0)
        disconnect(g_hitsort_globals.sock_listen);
        g_hitsort_globals.sock_listen = -1;

        fprintf('HitSort server stopped\n');
    end

    g_hitsort_globals.recv_buff = '';
end

