function hitsort_start_server()
%
% Start listening for incoming connections.
%
% Copyright 2019 Dakel.cz, www.dakel.cz
% Copyright 2019 Michal Hanak, hanak@dakel.cz
%
% license: BSD-3-Clause
%
    global g_hitsort_globals;

    g_hitsort_globals.recv_buff = '';

    if(g_hitsort_globals.sock_conn >= 0)
        fprintf("Socket already connected on port %d\n", g_hitsort_globals.server_port);
        return
    end

    # only create the main listening socket if not yet created
    if g_hitsort_globals.sock_listen < 0
        g_hitsort_globals.sock_listen = socket(AF_INET, SOCK_STREAM, 0);
        if g_hitsort_globals.sock_listen < 0
            fprintf("socket failed\n");
            return
        end

        if bind(g_hitsort_globals.sock_listen, g_hitsort_globals.server_port) < 0
            fprintf("bind failed\n");
            return
        end

        if listen(g_hitsort_globals.sock_listen, 1) < 0
            fprintf("listen failed\n");
            return
        end
    end

    printf("HitSort server listening on port %d\n", g_hitsort_globals.server_port);

    if g_hitsort_globals.sock_conn < 0
        g_hitsort_globals.sock_conn = accept(g_hitsort_globals.sock_listen);
        if g_hitsort_globals.sock_conn < 0
            fprintf("accept failed\n");
            return
        end
    end
end
