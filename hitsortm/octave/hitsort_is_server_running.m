function yes = hitsort_is_server_running()
%
% Test if our TCP server socket is open
%
% Copyright 2019 Dakel.cz, www.dakel.cz
% Copyright 2019 Michal Hanak, hanak@dakel.cz
%
% license: BSD-3-Clause
%
    global g_hitsort_globals;
    yes = g_hitsort_globals.sock_conn > 0;
end
