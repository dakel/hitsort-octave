function hitsort_send_data(data)
%
% Send TCP data
%
% Copyright 2019 Dakel.cz, www.dakel.cz
% Copyright 2019 Michal Hanak, hanak@dakel.cz
%
% license: BSD-3-Clause
%
    global g_hitsort_globals;
    send(g_hitsort_globals.sock_conn, data);
end
