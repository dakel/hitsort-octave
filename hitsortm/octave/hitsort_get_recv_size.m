function size = hitsort_get_recv_size()
%
% Determine how many bytes we may receive
%
% Copyright 2019 Dakel.cz, www.dakel.cz
% Copyright 2019 Michal Hanak, hanak@dakel.cz
%
% license: BSD-3-Clause
%
    global g_hitsort_globals;

    % Octave cannot determine number of TCP pending bytes, but it does not
    % block in recv() when more than available bytes are requested.
    % It only blocks when no bytes are received (which is okay for our usecase)
    if(g_hitsort_globals.sock_conn > 0)
        size = 1024*1024;
    else
        size = -1;
    end
end
