function data = hitsort_recv_data(size)
%
% Receive TCP data as string
%
% Copyright 2019 Dakel.cz, www.dakel.cz
% Copyright 2019 Michal Hanak, hanak@dakel.cz
%
% license: BSD-3-Clause
%
    global g_hitsort_globals;

    [one, len] = recv(g_hitsort_globals.sock_conn, size);

    if(len <= 0)
        % connection lost
        g_hitsort_globals.sock_conn = -1;
        data = '';
    else
        data = char(one);
    end
end
