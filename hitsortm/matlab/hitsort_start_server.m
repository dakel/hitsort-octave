function hitsort_start_server()
%
% Start listening for incoming connections.
%
% Copyright 2019 Dakel.cz, www.dakel.cz
% Copyright 2019 Michal Hanak, hanak@dakel.cz
%
% license: BSD-3-Clause
%
    global g_hitsort_globals;

    g_hitsort_globals.recv_buff = '';

    if(strcmp(g_hitsort_globals.tcp.Status, 'open'))
        fprintf('Socket already connected on port %d\n', g_hitsort_globals.tcp.RemotePort);

        % discard all characters pending in the socket
        if(g_hitsort_globals.tcp.BytesAvailable > 0)
            fread(g_hitsort_globals.tcp, g_hitsort_globals.tcp.BytesAvailable);
        end
    else
        % begin listening
        fprintf('HitSort server listening on port %d\n', g_hitsort_globals.tcp.RemotePort);
        fopen(g_hitsort_globals.tcp);
    end
end
