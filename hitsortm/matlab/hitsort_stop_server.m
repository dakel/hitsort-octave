function hitsort_stop_server()
%
% Terminate server and close sockets.
%
% Copyright 2019 Dakel.cz, www.dakel.cz
% Copyright 2019 Michal Hanak, hanak@dakel.cz
%
% license: BSD-3-Clause
%
    global g_hitsort_globals;

    if(strcmp(g_hitsort_globals.tcp.Status, 'open'))
        fclose(g_hitsort_globals.tcp);
    end

    fprintf('HitSort server stopped\n');
    g_hitsort_globals.recv_buff = '';
end

