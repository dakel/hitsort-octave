function data = hitsort_recv_data(size)
%
% Receive TCP data as string
%
% Copyright 2019 Dakel.cz, www.dakel.cz
% Copyright 2019 Michal Hanak, hanak@dakel.cz
%
% license: BSD-3-Clause
%
    global g_hitsort_globals;
    data = char(fread(g_hitsort_globals.tcp, size)'); %'
end
