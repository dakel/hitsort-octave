function size = hitsort_get_recv_size()
%
% Determine how many bytes we may receive
%
% Copyright 2019 Dakel.cz, www.dakel.cz
% Copyright 2019 Michal Hanak, hanak@dakel.cz
%
% license: BSD-3-Clause
%
    global g_hitsort_globals;
    size = g_hitsort_globals.tcp.BytesAvailable;
end
