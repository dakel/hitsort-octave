function hitsort_run_forever()
%
% Keep the server running all the time
%
% Copyright 2019 Dakel.cz, www.dakel.cz
% Copyright 2019 Michal Hanak, hanak@dakel.cz
%
% license: BSD-3-Clause
%
    while(1)
        hitsort_run();
    end
end
