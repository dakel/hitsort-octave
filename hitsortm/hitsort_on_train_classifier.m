function result = hitsort_on_train_classifier(params)
%
% Default hit training routine (this call is typically overriden).
% The ZDaemon client call the TrainClassifier() method to assign the hit 
% and given class number as a training value.
%
% Copyright 2019 Dakel.cz, www.dakel.cz
% Copyright 2019 Michal Hanak, hanak@dakel.cz
%
% license: BSD-3-Clause
%
% Input:
%     params.hit - Structure with hit parameters
%     params.signal - Signal data in a raw format selected by the ZDaemon (if selected).
%     params.signal_real - Signal data re-calculated by hitsort_recv_call to standard Octave/Matlab vector in voltage units.
%     params.train_class - Value 1...N used as a training value
%     params.cookie - Custom string value stored in the project
%
% Output:
%     result.success - Boolean training operation result.
%     result.cookie - Optional value to store to project settings.
%

    result.success = true;
    fprintf('WARNING: you should override the "hitsort_on_train_classifier" function in your Hit Sorter engine\n');
end
