function hitsort_run()
%
% This is the endless loop processing one session of the hitsort engine.
%
% Copyright 2019 Dakel.cz, www.dakel.cz
% Copyright 2019 Michal Hanak, hanak@dakel.cz
%
% license: BSD-3-Clause
%
    % listen and wait until client connects
    hitsort_start_server();

    % process calls as they are coming, until the Exit() JSON-RPC call
    while(hitsort_recv_call())
        ;
    end

    % close sockets and clean-up
    hitsort_stop_server();

    % unregister all existing engine paths
    hitsort_rm_engine_paths();
end
